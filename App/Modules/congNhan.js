const express = require("expess");

const ConNguoi = require("../Modules/conNguoi");

class CongNhan extends ConNguoi {
    constructor(hoTen, ngaySinh, queQuan, nganhNghe, noiLamViec, luong) {
        super (hoTen, ngaySinh, queQuan),
            this.nganhNghe = nganhNghe,
            this.noiLamViec = noiLamViec,
            this.luong = luong
    }
}

var congNhanKhuyenTb = new CongNhan("Trinh Bao Khuyen", "20/06/2000", "Long An", "IT", "Ironhack", "10 trieu");
console.log(congNhanKhuyenT);
console.log(congNhanKhuyenT instanceof ConNguoi);
console.log(congNhanKhuyenT instanceof CongNhan);


module.exports = CongNhan;