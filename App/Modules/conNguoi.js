const express = require("express");

class ConNguoi {
    constructor(hoTen, ngaySinh, queQuan) {
        this.hoTen = hoTen,
        this.ngaySinh = ngaySinh,
        this.queQuan = queQuan
    }
}

var khuyenTb = new ConNguoi("Trinh Bao Khuyen", "20/06/2000", "Long An");
console.log(khuyenTb);
console.log(khuyenTb instanceof ConNguoi);


module.exports = ConNguoi;