const express = require("expess");

const HocSinh = require("../Modules/hosSinh");

class SinhVien extends HocSinh {
    constructor(hoTen, ngaySinh, queQuan, tenTruong, lop, SDT, chuyenNganh, MSSV) {
        super (hoTen, ngaySinh, queQuan, tenTruong, lop, SDT),
            this.chuyenNganh = chuyenNganh,
            this.MSSV = MSSV
    }
}

var sinhVienKhuyenTb = new HocSinh("Trinh Bao Khuyen", "20/06/2000", "Long An", "UEH", "KM002", "0333142287", "Kinh Doanh Thuong Mai", "31181025011");
console.log(sinhVienKhuyenTb);
console.log(sinhVienKhuyenTb instanceof ConNguoi);
console.log(sinhVienKhuyenTb instanceof HocSinh);
console.log(sinhVienKhuyenTb instanceof SinhVien);


module.exports = SinhVien;