//import thu vien express js 
const express = require('express');

//Khoi tap express
const app = express();

//Khai bao port
const port = 8000;


app.use((req, res, next) => {
 let today = new Date();

 console.log("Current: ", today);

 next();
});


// Callback function
// là 1 function
// là 1 tham số của 1 hàm khác
// sẽ chạy sau khi hàm chủ thể được thực hiện
// Lắng nghe app trên cổng được khai báo 8000
app.listen(port, () => {
 console.log("App listening on port: ", port);
});

